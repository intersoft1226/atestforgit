# Data Collection Field validations covered for fields - Title, PostalCode, 
# Province, State, Phone, MOCode, FirstName, LastName, EmailConsent, Email, City, 
# BounceXFields, BabyField, Address1, Address2, MobilePhone, Zipcode
Feature: Validating Field Validation for Sign Up services of Data Collection template type.




@smokeTest1
Scenario Outline: DC001 - Data Collection Template Test- Validate the valid response when valid input parameters is provided.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName									  |
	|2        | 200  |SheetPath_DataCollection   			|SheetName_DataCollection     |
#	|5        | 200  |SheetPath_ProductionTemplate 		|SheetName_ProductionTemplate |

#
#Valid case- when Mobile Phone is mandatory in template


@smokeTest
Scenario Outline: DC002 - Data Collection Template Test- Validate the response when Blank Mobile Phone is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests with "blank" "mobilePhone" field 
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName					  				|Message			   			|
	|3        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOBILEPHONEREQUIRED  |
#	

@smokeTest
Scenario Outline: DC003 - Data Collection Template Test- Validate the response when Invalid Mobile Phone is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  		   |
	|4        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | MOBILEPHONEWITHLESSTHANMINLENGTH   |
	


@smokeTest
Scenario Outline: DC004 - Data Collection Template Test- Validate the response when Mobile Phone Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	    |
	|5        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOBILEPHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: DC005 - Data Collection Template Test- Validate the response when Mobile Phone less than Min Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  		   |
	|6        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOBILEPHONEWITHLESSTHANMINLENGTH    |
	
	
@smokeTest
Scenario Outline: DC006 - Data Collection Template Test- Validate the response when Mobile Phone having Two Hypens Invalid Format is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|7        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOBILEPHONEWITHTWOHYPENSINVALIDFORMAT    |
		

@smokeTest
Scenario Outline: DC007 - Data Collection Template Test- Validate the response when Mobile Phone having AlphaNumeric value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|8        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOBILEPHONEWITHALPHANUMERIC    |


@smokeTest
Scenario Outline: DC008 - Data Collection Template Test- Validate the response when Mobile Phone having 12Digit value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					 |
	|9        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOBILEPHONEWITH12DIGIT    |
	
	

@smokeTest
Scenario Outline: DC009 - Data Collection Template Test- Validate the response when address1 with Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					     |
	|10        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |ADDRESS1EXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: DC010 - Data Collection Template Test- Validate the response when address1 with Alphanumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message  |
	|11       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success |
	


@smokeTest
Scenario Outline: DC011 - Data Collection Template Test- Validate the response when address1 with SpecialCharacters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath											|SheetName									  |Message    |
	|12       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success   |	
	


@smokeTest
Scenario Outline: DC011 - Data Collection Template Test- Validate the response when address2 with Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath												|SheetName									  |Message					    				 |
	|13        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |ADDRESS2EXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: DC012 - Data Collection Template Test- Validate the response when address2 with Alphanumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message  |
	|14       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success |
	


@smokeTest
Scenario Outline: DC013 - Data Collection Template Test- Validate the response when address2 with SpecialCharacters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|15       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success   |	


	
	
@smokeTest
Scenario Outline: DC014 - Data Collection Template Test- Validate the response when babyField Exceeding MaxValue is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                       |
	|16       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | BABYFIELDEXCEEDINGMAXVALUE   |	
	
	
		
@smokeTest
Scenario Outline: DC015 - Data Collection Template Test- Validate the response when babyField With LessThan MinValue is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                          |
	|17       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | BABYFIELDWITHLESSTHANMINVALUE   |
	
# This will be valid if Baby Field is non mandatory		
#@smokeTest
#@currentTest1
#Scenario Outline: DC016 - Data Collection Template Test- Validate the response when babyField Accepting Zero is sent in request.
#	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  When user hit "post" requests
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message    |
#	|18       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success   |	
#	
#@currentTest		
#@smokeTest
#Scenario Outline: DC017 - Data Collection Template Test- Validate the response when babyField Count Less Than ChildrenCount is sent in request.
#	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
#	When  user hit "post" requests with BabyField less than children count
#	Then  User should received response status <code>
#	And   User validated the response <Message> received
#	
#	Examples:	
#	|Rowindex | code |SheetPath							|SheetName					  |Message                                |
#	|19       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | BABYFIELDCOUNTLESSTHANCHILDRENCOUNT   |		
	
		
@smokeTest
Scenario Outline: DC018 - Data Collection Template Test- Validate the response when CmpDeploymentStrategy field exceeds MaxLength is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                           |
	|20       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CMPDEPLOYMENTSTRATEGYMAXLENGTH   |		
	
	
		
@smokeTest
Scenario Outline: DC019 - Data Collection Template Test- Validate the response when CmpNameMaxLength field exceeds MaxLength is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message             |
	|21       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CMPNAMEMAXLENGTH   |	
	
		
@smokeTest
Scenario Outline: DC020 - Data Collection Template Test- Validate the response when CmpVariationID field exceeds MaxLength is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                    |
	|22       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CMPVARIATIONIDMAXLENGTH   |	
	
	
	
		
@smokeTest
Scenario Outline: DC021 - Data Collection Template Test- Validate the response when CmpConcept field exceeds MaxLength is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                |
	|23       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CMPCONCEPTMAXLENGTH   |	
	
	
	
@smokeTest
Scenario Outline: DC022 - Data Collection Template Test- Validate the response when CmpDevice field exceeds MaxLength is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message               |
	|24       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CMPDEVICEMAXLENGTH   |	
	
	
	
@smokeTest
Scenario Outline: DC023 - Data Collection Template Test- Validate the response when CmpFormat field exceeds MaxLength is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message               |
	|25       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CMPFORMATMAXLENGTH   |		
	
	
	
@smokeTest
Scenario Outline: DC024 - Data Collection Template Test- Validate the response when City field exceeds MaxLength is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                   |
	|26       |400  |SheetPath_DataCollection   		|SheetName_DataCollection     | CITYEXCEEDINGMAXLENGTH   |		
	
	
		
@smokeTest
Scenario Outline: DC025 - Data Collection Template Test- Validate the response when City Accepting Alphanumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|27       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success   |		
	
	
@smokeTest
Scenario Outline: DC027 - Data Collection Template Test- Validate the response when City Accepting SpecialCharacters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|28       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success   |		
	
		
@smokeTest
Scenario Outline: DC028 - Data Collection Template Test- Validate the response when email Without At The Rate is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                  |
	|29       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | EMAILWITHOUTATTHERATE   |	
	
		
@smokeTest
Scenario Outline: DC029 - Data Collection Template Test- Validate the response when email Without At Dot is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message            |
	|30       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | EMAILWITHOUTDOT   |	
	
	
	
		
@smokeTest
Scenario Outline: DC030 - Data Collection Template Test- Validate the response when email With No Prefix before At The Rate is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                             |
	|31       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | EMAILWITHNOPREFIXBEFOREATTHERATE   |
	
	
		
@smokeTest
Scenario Outline: DC031 - Data Collection Template Test- Validate the response when email With exceeding Max length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                               |
	|32       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | EMAILWITHEMAILIDEXCEEDINGMAXLENGTH   |	
	
		
@smokeTest
Scenario Outline: DC032 - Data Collection Template Test- Validate the response when email With Multiple At the rate is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                               |
	|33       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | EMAILWITHMULTIPLEATTHERATEINEMAIL    |	
	
		
@smokeTest
Scenario Outline: DC033 - Data Collection Template Test- Validate the response when email With SpecialCharacters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message                        |
	|34       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | EMAILWITHSPECIALCHARACTERS    |		
	
	
@smokeTest
Scenario Outline: DC034 - Data Collection Template Test- Validate the response when email With Spaces is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message            |
	|35       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | EMAILWITHSPACE    |	
	

		
@smokeTest
Scenario Outline: DC035 - Data Collection Template Test- Validate the response when email With Leading Spaces is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message     |
	|36       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success    |	
	
		
@smokeTest
Scenario Outline: DC036 - Data Collection Template Test- Validate the response when email With Trailing Spaces is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message     |
	|37       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success    |	
	
		
@smokeTest
Scenario Outline: DC037 - Data Collection Template Test- Validate the response when EMAILCONSENT2MANDATORY is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message            |
	|38       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     | EMAILCONSENT2MANDATORY    |							
	


@smokeTest
Scenario Outline: DC038 - Data Collection Template Test- Validate the response when FirstName with Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|39        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |FIRSTNAMEEXCEEDINGMAXLENGTH    |
	


@smokeTest
Scenario Outline: DC039 - Data Collection Template Test- Validate the response when FirstName with Alphanumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message  |
	|40       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success |
	


@smokeTest
Scenario Outline: DC040 - Data Collection Template Test- Validate the response when FirstName with SpecialCharacters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|41       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success   |	
	
	
						
@smokeTest
Scenario Outline: DC041 - Data Collection Template Test- Validate the response when LastName with Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|42        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |LASTNAMEEXCEEDINGMAXLENGTH     |
	


@smokeTest
Scenario Outline: DC042 - Data Collection Template Test- Validate the response when LastName with Alphanumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message  |
	|43       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success |
	


@smokeTest
Scenario Outline: DC043 - Data Collection Template Test- Validate the response when LastName with SpecialCharacters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|44       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     | success   |	
	
	

@smokeTest
Scenario Outline: DC044 - Data Collection Template Test- Validate the response when MoCode Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|45        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: DC045 - Data Collection Template Test- Validate the response when MOCode less than Min Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|46       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOCODELESSTHANMINIMUMLENGTH    |
	
	
@smokeTest
Scenario Outline: DC046 - Data Collection Template Test- Validate the response when MO Code Alphanumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|47        | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     |success    |
		

@smokeTest
Scenario Outline: DC047 - Data Collection Template Test- Validate the response when MOCode having SpecialCharacters value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|48        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOCODEWITHSPECIALCHARACTERS    |
	

@smokeTest
Scenario Outline: DC048 - Data Collection Template Test- Validate the response when MOCode having HyphenValidFormat value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|49        | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     |success    |	
	

@smokeTest
Scenario Outline: DC049 - Data Collection Template Test- Validate the response when MOCode having Hyphen InValidFormat value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	                        |
	|50        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |MOCODEWITHHYPHENINVALIDFORMAT    |	
		
#Mo Code ends	


@smokeTest
Scenario Outline: DC050 - Data Collection Template Test- Validate the response when Phone Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|51        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |PHONEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: DC051 - Data Collection Template Test- Validate the response when Phone less than Min Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|52       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |PHONEWITHLESSTHANMINLENGTH    |
	

@smokeTest
Scenario Outline: DC052 - Data Collection Template Test- Validate the response when phone With Two Hypens InvalidFormat is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|53        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |PHONEWITHTWOHYPENSINVALIDFORMAT    |
		

@smokeTest
Scenario Outline: DC053 - Data Collection Template Test- Validate the response when Phone having AlphaNumeric value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|54        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |PHONEWITHALPHANUMERIC    |
	

@smokeTest
Scenario Outline: DC054 - Data Collection Template Test- Validate the response when Phone having 12Digit value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|55        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |PHONEWITH12DIGIT    |	
	

@smokeTest
Scenario Outline: DC055 - Data Collection Template Test- Validate the response when Phone having Single Hyphen InValidFormat value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|56        | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     |success    |	
		
		

@smokeTest
Scenario Outline: DC056 - Data Collection Template Test- Validate the response when PostalCode Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|57        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |POSTALCODEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: DC057 - Data Collection Template Test- Validate the response when PostalCode with Invalid Format is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|58       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |POSTALCODEWITHINVALIDFORMAT    |
	


@smokeTest
Scenario Outline: DC058 - Data Collection Template Test- Validate the response when PostalCode With SpecialCharacters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|59        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |POSTALCODEWITHSPECIALCHARACTER    |
		


@smokeTest
Scenario Outline: DC059 - Data Collection Template Test- Validate the response when PostalCode with Invalid Format2 value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|60        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |POSTALCODEWITHINVALIDFORMAT2    |
	

@smokeTest
Scenario Outline: DC060 - Data Collection Template Test- Validate the response when PostalCode WITH INVALID FORMAT3 value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|61        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |POSTALCODEWITHINVALIDFORMAT3    |	
	
	

@smokeTest
Scenario Outline: DC061 - Data Collection Template Test- Validate the response when province Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|62        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |PROVINCEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: DC062 - Data Collection Template Test- Validate the response when province with AlphaNumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|63       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |PROVINCEACCEPTINGALPHANUMERIC    |
	


@smokeTest
Scenario Outline: DC063 - Data Collection Template Test- Validate the response when province With SpecialCharacters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|64        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |PROVINCEACCEPTINGSPECIALCHARACTERS    |
		

@smokeTest
Scenario Outline: DC064 - Data Collection Template Test- Validate the response when province with Invalid value value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|65        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |PROVINCEWITHINVALIDVALUE    |
	
	

@smokeTest
Scenario Outline: DC065 - Data Collection Template Test- Validate the response when State Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|66        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |STATEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: DC066 - Data Collection Template Test- Validate the response when state With LessThan Min Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|67       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |STATEWITHLESSTHANMINLENGTH    |
	


@smokeTest
Scenario Outline: DC067 - Data Collection Template Test- Validate the response when state With Non Capital Valid Value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|68        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |STATEWITHNONCAPITALVALIDVALUE    |
		


@smokeTest
Scenario Outline: DC068 - Data Collection Template Test- Validate the response when state With Special Characters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|69        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |STATEWITHSPECIALCHARACTERS    |
	

@smokeTest
Scenario Outline: DC069 - Data Collection Template Test- Validate the response when state With AlphaNumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|70        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |STATEWITHALPHANUMERIC    |	
	

@smokeTest
Scenario Outline: DC070 - Data Collection Template Test- Validate the response when PostalCode having Single Hyphen InValidFormat value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|71        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |STATEWITHNUMERICVALUE    |	
		
		
	

@smokeTest
Scenario Outline: DC071 - Data Collection Template Test- Validate the response when Title Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|72        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |TITLEEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: DC072 - Data Collection Template Test- Validate the response when title Accepting Alphanumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|73       | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     |success    |
	


@smokeTest
Scenario Outline: DC073 - Data Collection Template Test- Validate the response when title Accepting Special Characters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|74        | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     |success    |
		

	

@smokeTest
Scenario Outline: DC074 - Data Collection Template Test- Validate the response when zip Exceeding Max Length is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					   |
	|75        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |ZIPEXCEEDINGMAXLENGTH    |

	

@smokeTest
Scenario Outline: DC075 - Data Collection Template Test- Validate the response when zip With Less Than MaxLength is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					      |
	|76       | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |ZIPWITHLESSTHANMAXLENGTH    |
	

@smokeTest
Scenario Outline: DC076 - Data Collection Template Test- Validate the response when zip With CharAndDigit InValid Value is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  			    |
	|77        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |ZIPWITHCHARANDDIGITINVALIDVALUE    |
		


@smokeTest
Scenario Outline: DC077 - Data Collection Template Test- Validate the response when zip With Special Characters is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message					  	  |
	|78        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |ZIPWITHSPECIALCHARACTERS    |
	


@smokeTest
Scenario Outline: DC078 - Data Collection Template Test- Validate the response when zip With AlphaNumeric is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message	   |
	|79        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |ZIPWITHALPHANUMERIC    |	
	
	

@smokeTest
Scenario Outline: DC079 - Data Collection Template Test- Validate the response when zip With Numeric Value  is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|80        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |ZIPWITHNUMERICVALUE    |	
		
		
		

@smokeTest
Scenario Outline: DC080 - Data Collection Template Test- Validate the response when zip With Numeric And Char Value  is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|81        | 400  |SheetPath_DataCollection   		|SheetName_DataCollection     |ZIPWITHNUMERICANDCHARVALUE    |	
			
	
@smokeTest
Scenario Outline: DC081 - Data Collection Template Test- Validate the response when zip With 10Numeric Value  is sent in request.
	Given User want to hit "DataCollection" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
	When  When user hit "post" requests
	Then  User should received response status <code>
	And   User validated the response <Message> received
	
	Examples:	
	|Rowindex | code |SheetPath							|SheetName					  |Message    |
	|82        | 200  |SheetPath_DataCollection   		|SheetName_DataCollection     |success    |	
	

	
	
			
		
		