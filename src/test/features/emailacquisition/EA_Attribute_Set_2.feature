# Email Acquisition Field validations covered for fields - Children[1],Children[2],Children[3]																																						
Feature: Validating Field Validation of Sign Up services for Field "BabyField-Children" of template type Email Acquisition. 																																	
																																	
																															
@smokeTest																																	
Scenario Outline: EA082 - Email Acquisition Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex  | code |SheetPath											 |SheetName							  								 |Message					          			|		
	|83        | 400  |SheetPath_EmailAcquistion   		 |SheetName_EmailAcquistion_Attributes     |CHILDRENFIRSTNAMEUNKNOWNBLANK   |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA083 - Email Acquisition Template Test- Validate the response when blank FIRSTNAME of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName											  				 |Message								   |
	|84       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes      |CHILDRENFIRSTNAMEBLANK   |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA084 - Email Acquisition Template Test- Validate the response when blank HASDOB of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName								  		  				 |Message				 		  	 |			
	|85       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes      |CHILDRENHASDOBBLANK    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: EA085 - Email Acquisition Template Test- Validate the response when blank DATE OF BIRTH of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName											 						|Message				 				      |
	|86       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes   	  |CHILDRENDATEOFBIRTHBLANK 	  |																												
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: EA086 - Email Acquisition Template Test- Validate the response when DATEOF BIRTH of First children is  in FUTURE is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					  				 						 |Message				          			   |	
	|87       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes      |CHILDRENWITHFUTUREDATEOFBIRTH    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA087 - Email Acquisition Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName						  				  				 |Message				          		      |			
	|88       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes      |CHILDRENEXPECTEDBIRTHMONTHBLANK   |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA088 - Email Acquisition Template Test- Validate the response when ExpectedBirthYear = current year AND ExpectedMonth<currentMonth of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath									  |SheetName							  			  				 |Message				       				         |			
	|89       | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes      |CHILDRENWITHPASTEXPECTEDBIRTHMONTH   |																																
																																	
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: EA089 - Email Acquisition Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YEAR of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName										   				  |Message				              		 |					
	|90       | 400  |SheetPath_EmailAcquistion 	|SheetName_EmailAcquistion_Attributes     |CHILDRENEXPECTEDBIRTHYEARBLANK    |																															
																																	
																																		
@smokeTest																																	
Scenario Outline: EA090 - Email Acquisition Template Test- Validate the response when Future EXPECTED BIRTH YEAR of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					  					 					|Message				                    	|				
	|91       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR  |																														
																																	
# *****This is not implemented yet since cant able to set Null values																																	
#@smokeTest																																	
#Scenario Outline: EA091 - Email Acquisition Template Test- Validate the response when CHILDREN WITH GENDER NULL of First children is sent in request.																																	
#	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
#	When  When user hit "post" requests with "null" "gender" field																																
#	Then  User should received response status <code>																																
#	And   User validated the response <Message> received																																
#																																	
#	Examples:																																
#	|Rowindex | code |SheetPath							|SheetName					  |Message				       |																
#	|92       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHGENDERNULL_1    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA092 - Email Acquisition Template Test- Validate the response when Invalid Gender Value of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName					 					   					|Message				   				         |			
	|93       | 400  |SheetPath_EmailAcquistion   |SheetName_EmailAcquistion_Attributes     |CHILDRENWITHINVALIDGENDERVALUE    |																																
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: EA093 - Email Acquisition Template Test- Validate the response when ExpectedBirthMonth < 0 of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName									  				 |Message				                 		    |						
	|94       | 400  |SheetPath_EmailAcquistion  	|SheetName_EmailAcquistion_Attributes  |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH |																															
																																	
																																		
@smokeTest																																	
Scenario Outline: EA094 - Email Acquisition Template Test- Validate the response when ExpectedBirthMonth > 12 of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath								  			|SheetName					            				  |Message				                 				  |							
	|95       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: EA095 - Email Acquisition Template Test- Validate the response when ExpectedBirthYear < current year of First children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName									  				 |Message				            			     |			
	|96       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes  |CHILDRENWITHPASTEXPECTEDBIRTHMONTH   |																														
																																	
																																	
																																			
@smokeTest																																	
Scenario Outline: EA96 - Email Acquisition Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										  |SheetName					             					|Message					       					  |					
	|97      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENFIRSTNAMEUNKNOWNBLANK_1    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA097 - Email Acquisition Template Test- Validate the response when blank FIRSTNAME of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName										  				 |Message									   |	
	|98      | 400  |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes    |CHILDRENFIRSTNAMEBLANK_1   |																															
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA098 - Email Acquisition Template Test- Validate the response when blank HASDOB of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath						  				|SheetName					  					 					|Message				  				|																
	|99      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENHASDOBBLANK_1    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: EA099 - Email Acquisition Template Test- Validate the response when blank DATE OF BIRTH of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath						  					|SheetName					  					 					|Message				    			     |																
	|100      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENDATEOFBIRTHBLANK_1    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: EA100 - Email Acquisition Template Test- Validate the response when DATEOF BIRTH of Second children is in FUTURE is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath											|SheetName					 										  |Message				        				    |	
	|101      | 400  |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHFUTUREDATEOFBIRTH_1    |																															
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: EA101 - Email Acquisition Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath									|SheetName										  				 |Message				               				|				
	|102      | 400  |SheetPath_EmailAcquistion |SheetName_EmailAcquistion_Attributes    |CHILDRENEXPECTEDBIRTHMONTHBLANK_1   |																																
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA102 - Email Acquisition Template Test- Validate the response when ExpectedBirthYear = current year AND ExpectedMonth<currentMonth of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName									     					|Message				                       |																
	|103      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_1  |																														
																																	
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA103 - Email Acquisition Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YEAR of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName									  	 				  |Message				              			 |																
	|104      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENEXPECTEDBIRTHYEARBLANK_1    |																														
																																	

																																
@smokeTest																																	
Scenario Outline: EA104 - Email Acquisition Template Test- Validate the response when Future EXPECTED BIRTH YEAR of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName						  				 				  |Message				                   			 |																
	|105      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR_1    |																														
																																	
# *****This is not implemented yet since cant able to set Null values																																	
#@smokeTest																																	
#Scenario Outline: EA105 - Email Acquisition Template Test- Validate the response when CHILDREN WITH GENDER NULL of Second children is sent in request.																																	
#	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
#	When  When user hit "post" requests with "null" "gender" field																																
#	Then  User should received response status <code>																																
#	And   User validated the response <Message> received																																
#																																	
#	Examples:																																
#	|Rowindex | code |SheetPath							|SheetName					  |Message				       |																
#	|106       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHGENDERNULL_2    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: EA106 - Email Acquisition Template Test- Validate the response when Invalid Gender Value of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					  					 					|Message				               			 |																
	|107      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHINVALIDGENDERVALUE_1    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA107 - Email Acquisition Template Test- Validate the response when ExpectedBirthMonth < 0 of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					  					 				  |Message				                      			|																
	|108      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_1    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: EA108 - Email Acquisition Template Test- Validate the response when ExpectedBirthMonth > 12 of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex  | code |SheetPath											|SheetName					  										|Message				                      			|																
	|109       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_1    |																														
																																	

																																		
@smokeTest																																	
Scenario Outline: EA109 - Email Acquisition Template Test- Validate the response when ExpectedBirthYear < current year of Second children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					 										  |Message				                   			 |																
	|110      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_1    |																														
																																	
																																	
																															
# The below test scenarios were writter in last thats why S.No is not in serial.
																																																																																												
@smokeTest																																	
Scenario Outline: EA131 - Email Acquisition Template Test- Validate the response when blank FIRSTNAMEUNKNOWN of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												  |SheetName					             |Message					       					   |					
	|132      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENFIRSTNAMEUNKNOWNBLANK_2    |																														
																																	
																																	
																																		
@smokeTest																																	
Scenario Outline: EA132 - Email Acquisition Template Test- Validate the response when blank FIRSTNAME of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName										  |Message									   |	
	|133      | 400  |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes    |CHILDRENFIRSTNAMEBLANK_2    |																															
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: EA133 - Email Acquisition Template Test- Validate the response when blank HASDOB of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				    			 |																
	|134      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENHASDOBBLANK_2    |																														
																																	
																																		
@smokeTest																																	
Scenario Outline: EA134 - Email Acquisition Template Test- Validate the response when blank DATE OF BIRTH of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				      				  |																
	|135      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENDATEOFBIRTHBLANK_2    |																														
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: EA135 - Email Acquisition Template Test- Validate the response when DATEOF BIRTH of Third children is  in FUTURE is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					 						 |Message				        				     |	
	|136      | 400  |SheetPath_EmailAcquistion   	|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHFUTUREDATEOFBIRTH_2    |																															
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: EA136 - Email Acquisition Template Test- Validate the response when  EXPECTED BIRTH MONTH is BLANK of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath										|SheetName										  |Message				               				|				
	|137      | 400  |SheetPath_EmailAcquistion |SheetName_EmailAcquistion_Attributes    |CHILDRENEXPECTEDBIRTHMONTHBLANK_2    |																																
																																	
																																	
																																			
@smokeTest																																	
Scenario Outline: EA137 - Email Acquisition Template Test- Validate the response when ExpectedBirthYear = current year AND ExpectedMonth<currentMonth of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				                   				|																
	|138      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_2    |																														
																																	
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA138 - Email Acquisition Template Test- Validate the response when blank CHILDREN EXPECTED BIRTH YEAR of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					  					 |Message				             				  |																
	|139      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENEXPECTEDBIRTHYEARBLANK_2    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: EA139 - Email Acquisition Template Test- Validate the response when Future EXPECTED BIRTH YEAR of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                    |																
	|140       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR_2    |																														
																																	
																																

@smokeTest																																	
Scenario Outline: EA140 - Email Acquisition Template Test- Validate the response when CHILDREN WITH GENDER NULL of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  user hit "post" requests with "null" "gender" field	of "Third" children																														
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath											  |SheetName					  									  |Message				       			 |																
	|141      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHGENDERNULL_2    |																														
																																	
																																	
																																	
@smokeTest																																	
Scenario Outline: EA141 - Email Acquisition Template Test- Validate the response when Invalid Gender Value of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath												|SheetName					 											|Message				               		 |																
	|142      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHINVALIDGENDERVALUE_2  |																														
																																	
																																	
																																
@smokeTest																																	
Scenario Outline: EA142 - Email Acquisition Template Test- Validate the response when ExpectedBirthMonth < 0 of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |																
	|143       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_2    |																														
																																	
																																	
@smokeTest																																	
Scenario Outline: EA143 - Email Acquisition Template Test- Validate the response when ExpectedBirthMonth > 12 of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath							|SheetName					  |Message				                      |																
	|144       | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH_2    |																														
																																	
																																
@smokeTest																																	
Scenario Outline: EA144 - Email Acquisition Template Test- Validate the response when ExpectedBirthYear < current year of Third children is sent in request.																																	
	Given User want to hit "MobileAcquisition" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>																																
	When  When user hit "post" requests																																
	Then  User should received response status <code>																																
	And   User validated the response <Message> received																																
																																	
	Examples:																																
	|Rowindex | code |SheetPath													|SheetName					 						 |Message				                   				|																
	|145      | 400  |SheetPath_EmailAcquistion   		|SheetName_EmailAcquistion_Attributes     |CHILDRENWITHPASTEXPECTEDBIRTHMONTH_2    |																														
																																	
																																
																															