@instant_offer_Production_suite
Feature: Functional Validations of Instant Offer


  Scenario Outline: Instant Offer Prod 01 : Verify that offer get assigned to emails if use flags are true
    Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And User verify Response Page


    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        2 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |        3 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |        9 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |       10 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |       14 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |       15 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      

  Scenario Outline: Instant Offer Prod 02 : Verify that offer get assigned to inactive email if useLtiEmail flag is true
    Given User updated the field "email" in workbook "SheetName_InstantOfferProd" of file "SheetPath_InstantOfferProd" on row <Rowindex>
    Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "0" in results
    And User verify Response Page


    Examples: 
      | Rowindex | code | SheetPath              			| SheetName              |
      |        4 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |        5 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |        6 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |       11 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |       12 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd | 
      |       16 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |       17 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |       18 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |



  Scenario Outline: Instant Offer Prod 03 : Verify that offer not get assigned to new email if usenewEmail flag is true but sourceid:offfer is expired
    Given User updated the field "email" in workbook "SheetName_InstantOfferProd" of file "SheetPath_InstantOfferProd" on row <Rowindex>
    And User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "true" in results
    And User verify "offerStatusCd" flag of "myOffersResult" array  is "615" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        7 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |       13 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      |       19 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |

@1
  Scenario Outline: Instant Offer Prod 03 : Verify that offer not get assigned to existing email if useExistingEmail flag is false
   Given User want to hit "InstantOffer" template type SignUp service with template detail <Rowindex> from <SheetPath> and <SheetName>
    When When user hit "post" requests
    Then User should received response status <code>
    And User verify "isNewUser" flag of "emailResult" array  is "false" in results
    And User verify "" flag of "myOffersResult" array  is "null" in results
    And User verify Response Page

    Examples: 
      | Rowindex | code | SheetPath              | SheetName              |
      |        8 |  200 | SheetPath_InstantOfferProd | SheetName_InstantOfferProd |
      
