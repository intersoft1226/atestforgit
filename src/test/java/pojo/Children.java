package pojo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Children {
	
	private String firstNameUnknown;
	private String firstName;
	private String hasDateOfBirth;
	private String expectedBirthYear;
	private String expectedBirthMonth;
//	private int expectedBirthYear;
//	private int expectedBirthMonth;
	private String dateOfBirth;
	private String gender;
	
	
	public Children(String firstNameUnknown, String firstName, String hasDateOfBirth, String expectedBirthYear,
			String expectedBirthMonth, String dateOfBirth, String gender) {
		this.firstNameUnknown=firstNameUnknown;
		this.firstName=firstName;
		this.hasDateOfBirth=hasDateOfBirth;
		this.expectedBirthYear=expectedBirthYear;
		this.expectedBirthMonth=expectedBirthMonth;
		this.dateOfBirth=dateOfBirth;
		this.gender=gender;
		
		
	}
	public String getFirstNameUnknown() {
		return firstNameUnknown;
	}
	public void setFirstNameUnknown(String firstNameUnknown) {
		this.firstNameUnknown = firstNameUnknown;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getHasDateOfBirth() {
		return hasDateOfBirth;
	}
	public void setHasDateOfBirth(String hasDateOfBirth) {
		this.hasDateOfBirth = hasDateOfBirth;
	}
	public String getExpectedBirthYear() {
		return expectedBirthYear;
	}
	public void setExpectedBirthYear(String expectedBirthYear) {
		this.expectedBirthYear = expectedBirthYear;
	}
	public String getExpectedBirthMonth() {
		return expectedBirthMonth;
	}
	public void setExpectedBirthMonth(String expectedBirthMonth) {
		this.expectedBirthMonth = expectedBirthMonth;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	


}
