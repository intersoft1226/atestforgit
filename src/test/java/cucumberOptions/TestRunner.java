package cucumberOptions;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.testng.annotations.BeforeSuite;

import common.CommonLib;
import common.DataBaseConnection;
import common.FileLoggers;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(features = { "src/test/features" }, 
plugin = "json:target/jsonReports/cucumber-report.json", 
glue = {"stepDefinations"},tags = {"@1"})
public class TestRunner extends AbstractTestNGCucumberTests {
}

//,tags = {"@checkTest12"} 
//,tags="not @ignore"