package cap.edwfeed;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;

import org.apache.poi.util.SystemOutLogger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import common.DataBaseConnection;
import common.DataDriven;
import common.FileLoggers;
import common.Utils;
import io.restassured.path.json.JsonPath;

public class ValidateTerradataRecords extends Utils {
	
	DataBaseConnection dbTerraData;
	DataDriven edwrSheetreader;
	String edwApiRecordSheet;
	String edwTempalteSheet;
	HashMap<String, Object> excelApiRecords;
	String templateType;
	JSONObject payloadJson;
	String payload;
	String edwFeedExcelFilePath;
	String responseId;
	public void createTerradataConnection() {
		dbTerraData = new DataBaseConnection("terradata");

		
		try {
			edwFeedExcelFilePath=System.getProperty("user.dir") + getGlobalProperty("SheetPath_Saver");
			edwApiRecordSheet=getGlobalProperty("SheetName_Saver");
			 edwTempalteSheet=getGlobalProperty("SheetTemplate");
			edwrSheetreader= new DataDriven(edwFeedExcelFilePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int totalRows = edwrSheetreader.getRowCount(edwApiRecordSheet);
		//int rowNum =67;
		for (int rowNum =160; rowNum<=totalRows; rowNum++) {
			try {
				dbTerraData.dbConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			getTerradaatresult(rowNum);
			
			
			try {
				dbTerraData.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void getTerradaatresult(int row) {
		
		String terraQuery= generateQuery(row);
		//String terraQuery=" select * from CUST_TB.CUST_ACQ_PLTFRM_PRFL_ATRIB  where SRCSYS_CORRL_ID='dfd00115-2630-4564-9d75-073a21194c11' and  USER_DVC_TYPE_TXT is null order by Create_Ts desc";
		ResultSet terraData = null;
		terraData = dbTerraData.getSQLResults(terraQuery);
		if (Objects.nonNull(terraData)) {
			System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^teradata verification started^^^^^^^^^^^^^^^^^^^^^^");
			System.out.println("==================================================================================");
		}else {
			System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^teradata data not present^^^^^^^^^^^^^^^^^^^^^^");
		}
		
		HashMap<String, Object> terraRow = null;
		try {
			ResultSetMetaData  metadata=terraData.getMetaData();
			int columns = metadata.getColumnCount();
			terraRow = new HashMap<String, Object>();
			  while (terraData.next()) {
			     for (int i = 1; i <= columns; i++) {
			    	 terraRow.put(metadata.getColumnName(i), terraData.getString(i));
			     }
			  }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//validating custom fields
		Custom_Fields();
		
		//validating terradata 
		SoftAssert sf = new SoftAssert();
		for ( String key : excelApiRecords.keySet() ) {
			
			
			FileLoggers.info("Verifying-------"+key+"---------"+excelApiRecords.get(key)+ ":with:"+terraRow.get(key) );
			if((excelApiRecords.get(key)==null && terraRow.get(key)!=null) || (excelApiRecords.get(key)!=null && terraRow.get(key)==null)) {
				FileLoggers.info("value is mismatched");
				//sf.assertFalse(true);
				FileLoggers.info("Failed-------"+key+"---------"+excelApiRecords.get(key)+ ":with:"+terraRow.get(key) );
				Assert.assertTrue(false);
				continue;
			}
			else if (excelApiRecords.get(key)==null && terraRow.get(key)==null){
				Assert.assertTrue(true);
				//sf.assertTrue(true, "values are null");
				continue;
			} else if(String.valueOf(excelApiRecords.get(key)) == String.valueOf(terraRow.get(key)) ) {
				Assert.assertTrue(true);

				sf.assertEquals(excelApiRecords.get(key), terraRow.get(key));
			}else if(excelApiRecords.get(key).equals(terraRow.get(key))){
				Assert.assertTrue(true);

			}else if(excelApiRecords.get(key).toString().equals(terraRow.get(key))) {
				Assert.assertTrue(true);
			}else {
				FileLoggers.info("Failed-------"+key+"---------"+excelApiRecords.get(key)+ ":with:"+terraRow.get(key) );
				Assert.assertTrue(false);
			}
			
			
		}
		  sf.assertAll();
	}
	
	public String generateQuery(int row) {
		String customQuery=null;
		
		//run for row num given below
		//int row =28;
		
		//fetch the api data from edwfeed excel
		
		responseId = edwrSheetreader.getCellData(edwApiRecordSheet,"responseId",row);
		payload = edwrSheetreader.getCellData(edwApiRecordSheet,"Payload",row);
		String responseresult = edwrSheetreader.getCellData(edwApiRecordSheet,"Response",row);
		System.out.println("================================================================="+row);
		
		
		payloadJson = new JSONObject(payload);
		JsonPath responseJson = new JsonPath(responseresult);
		
		//-------------------------method tell you path exist in json or not 
		
		
	
		
		
		
		
		String TemplateName = payloadJson.getString("id");
		
		
		//get the template data 		
		//String templateData = edwrSheetreader.getCellData(edwTempalteSheet,"Response",2);		
			int templaterownum= edwrSheetreader.getCellRowNum(edwTempalteSheet, "TemplateName", TemplateName);
			templateType =edwrSheetreader.getCellData(edwTempalteSheet, "TemplateType", templaterownum);
			String templateDBData = edwrSheetreader.getCellData(edwTempalteSheet,"ResultSet",templaterownum);
			JSONObject templateDBDataJson = new JSONObject(templateDBData);
			
			excelApiRecords = new HashMap<String, Object >();
			
			//--------------------below items added from Payload 
			excelApiRecords.put("TEMPL_ID",  payloadJson.has("id")?payloadJson.getString("id"):null);
			excelApiRecords.put("USER_DVC_TYPE_TXT", payloadJson.has("CmpDevice")?payloadJson.getString("CmpDevice"):null);
			excelApiRecords.put("CAP_CMP_DPLOYMT_STRATG_TXT", payloadJson.has("CmpDeploymentStrategy")?payloadJson.getString("CmpDeploymentStrategy"):null);
			excelApiRecords.put("CAP_CMP_NM", payloadJson.has("CmpName")?payloadJson.getString("CmpName"):null);
			excelApiRecords.put("CAP_CMP_VARITN_ID", payloadJson.has("CmpVariationID")?payloadJson.getString("CmpVariationID"):null);
			excelApiRecords.put("ELECTRONIC_ADDRESS_TXT", payloadJson.has("email")?payloadJson.getString("email"):null);
			excelApiRecords.put("INSTNT_OFFER_SRC_ID", payloadJson.has("sourceID")?payloadJson.getString("sourceID"):null);
			//excelApiRecords.put("MO_CODE", payloadJson.has("mocode")?payloadJson.getString("mocode"):null);
			excelApiRecords.put("FRST_NM", payloadJson.has("firstName")?payloadJson.getString("firstName"):null);
			excelApiRecords.put("LAST_NM", payloadJson.has("lastName")?payloadJson.getString("lastName"):null);
			excelApiRecords.put("ADDRESS_LINE_1_TXT", payloadJson.has("addr1")?payloadJson.getString("addr1"):"unknown");
			excelApiRecords.put("ADDRESS_LINE_2_TXT", payloadJson.has("addr2")?payloadJson.getString("addr2"):"");
			excelApiRecords.put("CITY_NAME", payloadJson.has("city")?payloadJson.getString("city"):"UNKNOWN");
			//excelApiRecords.put("TELEPHONE_ADDRESS_ID", payloadJson.has("mobilePhone")?payloadJson.getString("mobilePhone"):null);
			
			
			
			//--------------------below items added from response 			


			//System.out.println("???????"+responseJson.get("results.emailResult.success"));
			excelApiRecords.put("RSPNS_STATUS_CODE",Objects.nonNull(responseJson.get("statusCode"))?responseJson.get("statusCode"):null);
			excelApiRecords.put("RSPNS_URL_TXT",Objects.nonNull(responseJson.get("responsePage"))?responseJson.getString("responsePage"):null);
			excelApiRecords.put("ERR_MSG_TXT", Objects.nonNull(responseJson.get("results.validationResult.errorMessages[0]"))?responseJson.getString("results.validationResult.errorMessages"):null);
			excelApiRecords.put("SRCSYS_CORRL_ID", Objects.nonNull(responseJson.get("responseId"))?responseJson.getString("responseId"):null);
			excelApiRecords.put("EML_NEW_USER_IND", Objects.nonNull(responseJson.get("results.emailResult.isNewUser"))?(responseJson.getBoolean("results.emailResult.isNewUser")?"Y":"N"):"N");
	
			excelApiRecords.put("EML_LGTRM_INACT_IND", Objects.nonNull(responseJson.get("results.emailResult.isLti"))?(responseJson.getBoolean("results.emailResult.isLti")?"Y":"N"):"N");
			excelApiRecords.put("EML_SPAM_IND", Objects.nonNull(responseJson.get("results.emailResult.isSpam"))?(responseJson.getBoolean("results.emailResult.isSpam")?"Y":"N"):"N");
			excelApiRecords.put("EML_UNDLVRABLE_IND", Objects.nonNull(responseJson.get("results.emailResult.isUndel"))?(responseJson.getBoolean("results.emailResult.isUndel")?"Y":"N"):"N");
			excelApiRecords.put("EML_OPTOUT_IND", Objects.nonNull(responseJson.get("results.emailResult.isOptIn"))?(responseJson.getBoolean("results.emailResult.isOptIn")?"Y":"N"):"N");
			//excelApiRecords.put("EVS_API_SCSS_FLG", Objects.nonNull(responseJson.get("results.emailResult.success"))?(responseJson.getBoolean("results.emailResult.success")?"Y":"N"):"N");
			excelApiRecords.put("MO_API_ERR_MSG_TXT", Objects.nonNull(responseJson.get("results.myOffersResult.errorMessages"))?responseJson.getString("results.myOffersResult.errorMessages[0]"):null);
			excelApiRecords.put("MO_API_SCSS_FLG", Objects.nonNull(responseJson.get("results.myOffersResult.success"))?(responseJson.getBoolean("results.myOffersResult.success")?"Y":"N"):"N");
			excelApiRecords.put("MO_API_OFFER_STATUS_CODE", Objects.nonNull(responseJson.get("results.myOffersResult.offerStatusCd"))?responseJson.getInt("results.myOffersResult.offerStatusCd"):null);
			excelApiRecords.put("MO_API_OFFER_DESC_TXT",  Objects.nonNull(responseJson.get("results.myOffersResult.offerDesc"))?responseJson.getString("results.myOffersResult.offerDesc"):null);
			excelApiRecords.put("MBL_NEWTOFILE_IND",  Objects.nonNull(responseJson.get("results.smsResult.isNewUser"))?(responseJson.getBoolean("results.smsResult.isNewUser")?"Y":"N"):"N");
			excelApiRecords.put("MBL_VENDOR_SCSS_FLG",  Objects.nonNull(responseJson.get("results.smsResult.success"))?(responseJson.getBoolean("results.smsResult.success")?"Y":"N"):"N");
			
			
			//--------------------below items added from database 	
			excelApiRecords.put("TEMPL_TYPE_TXT",  templateDBDataJson.has("templateType")?templateDBDataJson.getString("templateType"):null);
			excelApiRecords.put("RECORD_SOURCE_CD",  templateDBDataJson.has("acquisitionSource")?templateDBDataJson.getInt("acquisitionSource"):null);
			excelApiRecords.put("SITE_TXT",  templateDBDataJson.has("siteid")?templateDBDataJson.getString("siteid"):null);
			excelApiRecords.put("EML_NEWTOFILE_USE_REQ_IND",  templateDBDataJson.has("useEmailNewToFileFlag")?(templateDBDataJson.getBoolean("useEmailNewToFileFlag")?"Y":"N"):"N");
			excelApiRecords.put("EML_SPAM_USE_REQ_IND",  templateDBDataJson.has("useEmailSpamFlag")?(templateDBDataJson.getBoolean("useEmailSpamFlag")?"Y":"N"):"N");
			excelApiRecords.put("EML_UNDLVRABLE_USE_REQ_IND",  templateDBDataJson.has("useEmailUndelFlag")?(templateDBDataJson.getBoolean("useEmailUndelFlag")?"Y":"N"):"N");
			excelApiRecords.put("EML_LGTRM_INACT_USE_REQ_IND",  templateDBDataJson.has("useEmailLtiFlag")?(templateDBDataJson.getBoolean("useEmailLtiFlag")?"Y":"N"):"N");
			excelApiRecords.put("ESP_APIERR_USE_REQ_IND",  templateDBDataJson.has("useEspApiErrorFlag")?(templateDBDataJson.getBoolean("useEspApiErrorFlag")?"Y":"N"):"N");
			excelApiRecords.put("EML_EXSTGTOFILE_USE_REQ_IND",  templateDBDataJson.has("useEmailExistingToFileFlag")?(templateDBDataJson.getBoolean("useEmailExistingToFileFlag")?"Y":"N"):"N");
			
			excelApiRecords.put("PRGS_TO_OPTIN_TXT",  templateDBDataJson.has("programsToOptIn")?templateDBDataJson.getString("programsToOptIn"):null);
			//excelApiRecords.put("SIGNUP_RECVD_DT",  templateDBDataJson.has("programsToOptIn")?templateDBDataJson.getString("programsToOptIn"):null);
			//excelApiRecords.put("SIGNUP_RECVD_TM",  templateDBDataJson.has("programsToOptIn")?templateDBDataJson.getString("programsToOptIn"):null);
			
			
			
			//--------------------------------static data 
			excelApiRecords.put("MBL_VENDOR_ORIGN_NM",null);
			excelApiRecords.put("RI_ID",null);
			
				
//creating custom query by iterating each entry set 			
			customQuery= " select * from CUST_TB.CUST_ACQ_PLTFRM_PRFL_ATRIB pfile \r\n"
					+ "INNER JOIN  EDW_MCF_VW.ELECTRONIC_ADDRESS email on pfile.EMAIL_ADDRESS_ID = email.ELECTRONIC_ADDRESS_ID \r\n"
					+ "INNER JOIN  EDW_MCF_VW.MAILING_ADDRESS mail on pfile.MAILING_ADDRESS_ID = mail.MAILING_ADDRESS_ID\r\n"
					+ "INNER JOIN  EDW_MCF_VW.City city on mail.CITY_CD = city.CITY_CD\r\n"
					+ "where SRCSYS_CORRL_ID='"+responseJson.getString("responseId")+"' ";
			
			
		return customQuery;
		
	
	}
	
	
	
	public void Custom_Fields() {
		

		JsonPath customPayload = new JsonPath(payload);
		 JSONObject jsonResult = new JSONObject(payload);
		 // Get the result object
		 JSONArray customArray = jsonResult.getJSONArray("custom");
		
		
		
		HashMap<String, Object> CustomRecords = new HashMap<String, Object>();
		
		int babyCount = 0;
		try {
			babyCount = customPayload.getInt("babyFields");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			babyCount = 0;
		}
		System.out.println(customPayload.toString());
		
		if(babyCount!=0) {
			for (int i=0;i<babyCount;i++) {
				int j=i+1;
				CustomRecords.put("child["+j+"].gender", customPayload.get("children["+i+"].gender"));
				CustomRecords.put("child["+j+"].firstName", customPayload.get("children["+i+"].firstName"));
							
				String string = customPayload.get("children["+i+"].dateOfBirth");			
				LocalDate date = LocalDate.parse(string);
				
				CustomRecords.put("child["+j+"].birthDay",String.valueOf(date.getDayOfMonth()) );
				CustomRecords.put("child["+j+"].birthMonth", String.valueOf(date.getMonthValue()));
				CustomRecords.put("child["+j+"].birthYear", String.valueOf(date.getYear()));
			}
		}
		for(int i=0;i<customArray.length();i++) {
			System.out.println(customArray.get(i));
			String S =String.valueOf(customArray.get(i));
			JSONObject customInner = new JSONObject(S);
			CustomRecords.put(customInner.getString("id"), customInner.getString("value"));
			System.out.println(customInner.getString("id"));
			System.out.println(customInner.getString("value"));
		}
		
		
	
		String queryForCustomFields= " select custom.* from CUST_TB.CUST_ACQ_PLTFRM_PRFL_ATRIB pfile RIGHT JOIN  CUST_TB.CUST_ACQ_PLTFRM_PREFRC custom on pfile.CUST_ACQ_PLTFRM_CORRL_ID = custom.CUST_ACQ_PLTFRM_CORRL_ID where SRCSYS_CORRL_ID='"+responseId+"'"; 
				//"select * from CUST_TB.CUST_ACQ_PLTFRM_PREFRC   where CUST_ACQ_PLTFRM_CORRL_ID = '5727'";
		
		ResultSet customData = null;
		customData = dbTerraData.getSQLResults(queryForCustomFields);
		
		HashMap<String, Object> custRow = null;
		try {
			ResultSetMetaData  metadata=customData.getMetaData();
			
			int columns = metadata.getColumnCount();
			custRow = new HashMap<String, Object>();
			  while (customData.next()) {
			    	 custRow.put(customData.getString("PREFRC_KEY_NAME"), customData.getString("PREFRC_VAL"));
			  }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//validating custom columns
		
		for ( String key : CustomRecords.keySet() ) {

			FileLoggers.info("Verifying-------"+key+"---------"+CustomRecords.get(key)+ ":with:"+custRow.get(key) );
			if((CustomRecords.get(key)==null && custRow.get(key)!=null) || (excelApiRecords.get(key)!=null && custRow.get(key)==null)) {
				FileLoggers.info("value is mismatched");
				
				continue;
			}
			else if (CustomRecords.get(key)==null && custRow.get(key)==null){
				FileLoggers.info("CAP Data Matched with Terra data");
				FileLoggers.info("--------------------------------");
				Assert.assertTrue(true);
				continue;
			} else {
				Assert.assertEquals(CustomRecords.get(key), custRow.get(key));
				FileLoggers.info("CAP Data Matched with Terra data");
				FileLoggers.info("--------------------------------");
				
			}
			
			
		}
		
	}

	
}
