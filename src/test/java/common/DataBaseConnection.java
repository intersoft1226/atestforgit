package common;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class DataBaseConnection extends Utils{
	String url;
	String databaseName;
	//while creating object pass value "m2m" or "terradata"
	public DataBaseConnection(String databaseName) {
		System.out.println(databaseName);
		this.databaseName=databaseName;
	}

	String username;
	String password;
	ResultSet rs;
	public static Connection conn=null;
	public void dbConnection() throws IOException {	
		
		//if(!CommonLib.EdwDBFlag.equalsIgnoreCase("True"))
		if(databaseName.equalsIgnoreCase("m2m")){
			url= getGlobalProperty("DatabaseUrl");		
			username = getGlobalProperty("DatabaseUserName");
			password = getGlobalProperty("DatabasePassword");
		}
		else if(databaseName.equalsIgnoreCase("terradata")){
			 url= getGlobalProperty("TerraDatabaseUrl");		
			 username = getGlobalProperty("TerraDatabaseUserName");
			 password = getGlobalProperty("TerraDatabasePassword");
		}else if(databaseName.equalsIgnoreCase("MO")) {
			 url= getGlobalProperty("MODatabaseUrl");		
			 username = getGlobalProperty("MODatabaseUserName");
			 password = getGlobalProperty("MODatabasePassword");
		}
		
		System.out.println(url);
	        try {
	            conn = DriverManager.getConnection(url, username, password);
	            FileLoggers.info("Database connected successfully");           

	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	            throw new RuntimeException(e);	           
	        }	       
			
	    }
		
	public ResultSet getSQLResults(String query) {
		
		if (conn != null) {	             
        	
            //ResultSet rs;
			try {
				Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                    ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	                    
            return rs;
        } else {
            System.out.println("Failed to execute query");
            return null;
        }
		
	}
	
	public void closeConnection() throws SQLException{
		conn.close();
	}
}
