package common;

import java.io.IOException;
//import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.ObjectMapper;

import fieldValidations.Address1;
import fieldValidations.Address2;
import fieldValidations.BabyField;
import fieldValidations.BounceXCustomFields;
import fieldValidations.Children;
import fieldValidations.City;
import fieldValidations.CustomFields;
import fieldValidations.Email;
import fieldValidations.EmailConsent2;
import fieldValidations.FirstName;
import fieldValidations.Guardian;
import fieldValidations.LastName;
import fieldValidations.MoCode;
import fieldValidations.MobilePhone;
import fieldValidations.Phone;
import fieldValidations.PostalCode;
import fieldValidations.Province;
import fieldValidations.State;
import fieldValidations.Title;
import fieldValidations.ZipCode;
//import pojo.Custom;
import gherkin.deps.com.google.gson.Gson;
import pojo.EmailAcquisition;

public class Test {
	String updatedPayload = null;
	String fieldName;

	public String replacePayload(String payload, String fieldName, String fieldValue) throws JsonProcessingException {
		Gson gson = new Gson();
		gson.fromJson(payload, EmailAcquisition.class);
		EmailAcquisition strToJson = gson.fromJson(payload, EmailAcquisition.class);
		this.fieldName = fieldName;
		switch(fieldName) {
		case "email":
			strToJson.setEmail(fieldValue);
			break;
		case "id":
			break;
		case "addr1":
			strToJson.setAddr1(fieldValue);
			break;
		case "addr2":
			strToJson.setAddr2(fieldValue);
			break;
		case "firstName":
			strToJson.setFirstName(fieldValue);
			break;
		case "lastName":
			strToJson.setLastName(fieldValue);
			break;
		case "city":
			strToJson.setCity(fieldValue);
			break;
		case "postalCode":
			strToJson.setPostalCode(fieldValue);
			break;
		case "province":
			strToJson.setProvince(fieldValue);
			break;
		case "state":
			strToJson.setState(fieldValue);
			break;
		case "title":
			strToJson.setTitle(fieldValue);
			break;
		case "phone":
			strToJson.setPhone(fieldValue);
			break;
		case "mocode":
			strToJson.setMocode(fieldValue);
			break;
		case "mobilePhone":
			strToJson.setMobilePhone(fieldValue);
			break;
		case "zipCode":
			strToJson.setZipcode(fieldValue);
			break;
		case "babyFields":
			int value = Integer.parseInt(fieldValue);
			strToJson.setBabyFields(value);
			break;
		case "Children_firstName":
			List<pojo.Children> listA = strToJson.getChildren();
			listA.get(0).setFirstName(fieldValue);
			break;
		case "firstNameUnknown":
			List<pojo.Children> listB = strToJson.getChildren();
			listB.get(0).setFirstNameUnknown(fieldValue);
		case "hasDateOfBirth":
			List<pojo.Children> listC = strToJson.getChildren();
			listC.get(0).setHasDateOfBirth(fieldValue);
		case "dateOfBirth":
			List<pojo.Children> listD = strToJson.getChildren();
			listD.get(0).setDateOfBirth(fieldValue);
		case "expectedBirthYear":
			List<pojo.Children> listE = strToJson.getChildren();	
			listE.get(0).setExpectedBirthYear(fieldValue);
		case "expectedBirthMonth":
			List<pojo.Children> listF = strToJson.getChildren();
			listF.get(0).setExpectedBirthMonth(fieldValue);
		case "gender":
			List<pojo.Children> listG = strToJson.getChildren();
			listG.get(0).setGender(fieldValue);
		case "emailconsent2":
			strToJson.setEmailconsent("");		
			strToJson.setEmailconsent2("");
			break;
		case "CmpName":
			strToJson.setCmpName(fieldValue);
			break;
		case "CmpDeploymentStrategy":
			strToJson.setCmpDeploymentStrategy(fieldValue);		
			break;
		case "CmpVariationID":
			strToJson.setCmpVariationID(fieldValue);
			break;
		case "CmpConcept":
			strToJson.setCmpConcept(fieldValue);
			break;
		case "CmpFormat":
			strToJson.setCmpFormat(fieldValue);
			break;
		case "CmpDevice":
			strToJson.setCmpDevice(fieldValue);
			break;
		case "guardian":
			strToJson.setGuardian(fieldValue);
			break;
		}		
		ObjectMapper obj = new ObjectMapper();
		obj.setSerializationInclusion(Include.NON_NULL);
		obj.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		obj.registerModule(new Hibernate4Module());
		updatedPayload = obj.writerWithDefaultPrettyPrinter().writeValueAsString(strToJson);
		FileLoggers.info(updatedPayload);
		System.out.println("Updated Json is " + updatedPayload);
		return updatedPayload;
	}
}