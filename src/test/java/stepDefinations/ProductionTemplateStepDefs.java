package stepDefinations;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import cap.edwfeed.ApiDataSaver;
import common.CommonLib;
import common.DataBaseConnection;
import common.DataDriven;
import common.DataSet;
import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.TemplateValidation;
import common.Utils;
import common.ValidationMessages;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import cap.edwfeed.*;
//import m2m.EDWFeed.ValidateTerradataRecords;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import fieldValidations.BabyField;
import fieldValidations.UnPrintableCharacters;

public class ProductionTemplateStepDefs extends Utils {
	CommonLib reqspec1;
	String fieldValue;
	Response response;
	String scenarioName;
	CommonLib payloads;
	String methodName;
	String templateType;
	String sheetPath;
	String sheetName;
	Integer rowIndex;
	int Flag;
	DataSet ds = new DataSet();
	DataDriven excelWriter;
	SoftAssert softassert = new SoftAssert();
	String ResponsePage;
	String ExpectedErrorMessage;
	io.cucumber.core.api.Scenario scenario;

	@io.cucumber.java.Before
	public void before(io.cucumber.core.api.Scenario scenario) {
		this.scenario = scenario;
		scenarioName = scenario.getName();
	}

	public ProductionTemplateStepDefs() {
	}

	@Given("^User want to hit Sign up service for template (.+) from (.+) and (.+)$")
	public void user_want_to_hit_sign_up_service_for_template_from_and(Integer rowIndex, String sheetPath,
			String sheetName) throws Throwable {
		FileLoggers.initLogger();
		FileLoggers.startTestCase(scenarioName + "  " + rowIndex);
		CommonLib.sheetPath = System.getProperty("user.dir") + getGlobalProperty(sheetPath);
		System.out.println(CommonLib.sheetPath);
		CommonLib.sheetName = getGlobalProperty(sheetName);
		CommonLib.rowIndex = rowIndex;
		CommonLib.payloads = ds.setData(CommonLib.sheetPath, CommonLib.sheetName, CommonLib.rowIndex);
		RequestAPI req = new RequestAPI();
		CommonLib.res = req.getRequestSpecification(CommonLib.payloads);
		scenario.write("***************  Request Payload below: *************");
		scenario.write(CommonLib.payloads);

	}

	@When("When user hit {string} requests")
	public void when_user_hit_requests(String methodName) {
		List<String> data = ds.getDataList(CommonLib.sheetPath, CommonLib.sheetName, CommonLib.rowIndex);
		this.templateType = data.get(0);
		CommonLib.templateType = this.templateType;
		this.methodName = methodName;
		ResponseAPI resp = new ResponseAPI();
		CommonLib.response = resp.hitRequest(this.templateType, this.methodName, CommonLib.res);
		FileLoggers.info(" **********  Reponse Message is below   ********");
		FileLoggers.info(CommonLib.response.asString());
		scenario.write("Response received is below:");
		scenario.write("*********************  "+CommonLib.response.asString()+"     *****************");
		ApiDataSaver saver =new ApiDataSaver();
		saver.requestResponseSaver(CommonLib.payloads, CommonLib.response.asString());
	}

	@Then("User should received response status (.+)$")
	public void user_should_received_response_status(int statuscode) {
		System.out.println(CommonLib.response.asString());
		System.out.println(CommonLib.response.getStatusCode());
		// Validating Status Code
		softassert.assertEquals(CommonLib.response.getStatusCode(), statuscode);
		softassert.assertAll();
	}

	@And("^User should validate all Field validations$")
	public void user_should_validate_all_field_validations() throws Throwable {
		FileLoggers.info("******** Validating All Field Validations **********");
		TemplateValidation templateValidation = new TemplateValidation();
		templateValidation.fieldValidation(methodName, templateType, CommonLib.payloads, CommonLib.sheetPath,
				CommonLib.sheetName, CommonLib.rowIndex, this.scenario);
		FileLoggers.endTestCase();
	}

	@And("^User validated the response (.+) received$")
	public void user_validated_the_response_received(String message) throws Throwable {
		String ResponseReceived = CommonLib.response.asString();
		if (!message.contains("CUSTOM")) {
			if (!message.equalsIgnoreCase("success")) {
				ValidationMessages message1 = ValidationMessages.valueOf(message);
				ExpectedErrorMessage = message1.getValidationMessage();
			} else if (message.contains("CUSTOM")) {
				ExpectedErrorMessage = message;
				FileLoggers.info(ExpectedErrorMessage);
			} else {
				ExpectedErrorMessage = message;
			}
		} else {
			ValidationMessages message1 = ValidationMessages.valueOf(message);
			String ErrorMessage = message1.getValidationMessage();

			// ********* Fetching Custom fields Label *****************
			TemplateTypeStepDefs template = new TemplateTypeStepDefs();
			if(CommonLib.customFieldData==null) {
				fetchCustomFieldLabelData();
			}
			CommonLib.customFieldLabel = template.getCustomFieldLabelName(CommonLib.customFieldId);

			if (message.contains("BLANKFIELDID")) {
//				CommonLib.customFieldErrorMessage = CommonLib.customFieldId.concat(ErrorMessage);
				CommonLib.customFieldErrorMessage = CommonLib.customFieldLabel.concat(ErrorMessage);
			} else if (message.contains("BLANKFIELDVALUE")) {
//				CommonLib.customFieldErrorMessage = CommonLib.customFieldId.concat(ErrorMessage);
				CommonLib.customFieldErrorMessage = CommonLib.customFieldLabel.concat(ErrorMessage);
			} else if (message.contains("INVALIDFIELDVALUE")) {
//				CommonLib.customFieldErrorMessage = CommonLib.customFieldId.concat(ErrorMessage);
				CommonLib.customFieldErrorMessage = CommonLib.customFieldLabel.concat(ErrorMessage);
			} else if (message.contains("CUSTOMNONMANDATORYBLANKID")) {
				CommonLib.customFieldErrorMessage = ErrorMessage;
			}
			this.ExpectedErrorMessage = CommonLib.customFieldErrorMessage;
		}
		responseDataValidation(CommonLib.payloads, CommonLib.templateType, ResponseReceived, ExpectedErrorMessage);
	}

	@Then("^User should receive response status (.+) and Field validation message for \"([^\"]*)\" field$")
	public void user_should_receive_response_status_and_field_validation_message_for_something_field(
			int ExpectedStatusCode, String FieldName) throws Throwable {
		FileLoggers.info("******** Validating UnPrintable Characters Field Validations **********");
		String ExpectedErrorMessage = "The following field(s) - First Name - contain unprintable character(s)";
		String ActualResponse = CommonLib.response.asString();
		JSONObject jsobj = new JSONObject(CommonLib.payloads);
		String FieldWithSpecialCharacter = jsobj.getString("firstName");
		FileLoggers.info(
				"******Validating Special Character in Field Name " + FieldWithSpecialCharacter + "******************");
		responseDataValidation(CommonLib.payloads, templateType, ActualResponse, ExpectedErrorMessage);
	}

	// When statement when user hit request with blank or null value
	@When("^When user hit \"([^\"]*)\" requests with \"([^\"]*)\" \"([^\"]*)\" field$")
	public void when_user_hit_something_requests_with_something_something_field(String methodName, String value,
			String fieldName) throws Throwable {
		String updatedPayload;
		List<String> data = ds.getDataList(CommonLib.sheetPath, CommonLib.sheetName, CommonLib.rowIndex);
		CommonLib.templateType = data.get(0);
		if (value.equalsIgnoreCase("blank")) {
			fieldValue = "    ";
		} else {
			fieldValue = null;
		}
		ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
		updatedPayload = pup.replaceFieldValue(CommonLib.payloads, fieldName, fieldValue);
		FileLoggers.info("**********Updated Payload***");
		FileLoggers.info(updatedPayload);
		RequestAPI req = new RequestAPI();
		CommonLib.res = req.getRequestSpecification(updatedPayload);
		ResponseAPI resp = new ResponseAPI();
		CommonLib.response = resp.hitRequest(CommonLib.templateType, methodName, CommonLib.res);
		FileLoggers.info(" **********  Reponse Message is below   ********");
		FileLoggers.info(CommonLib.response.asString());
		scenario.write("*************Response received is below: *****************");
		scenario.write("*********************  "+CommonLib.response.asString()+"     *****************");

	}

	@Then("User verify {string} flag of {string} array  is {string} in results")
	public void user_verify_flag_of_array_is_in_results(String flagToCheck, String flagContainer, String expectedFlag) {
		String responseReceived = CommonLib.response.asString();
		flagValidation(templateType, responseReceived, flagToCheck, flagContainer, expectedFlag);

	}

	@Then("user is waiting for {int} seconds")
	public void user_is_waiting_for_seconds(Integer waitTime) {
		try {
			Thread.sleep(waitTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@And("User validated the (.+) status$")
	public void user_validated_the_smsresult_received(Boolean isnewuser) throws Throwable {
		String ActualResponse = CommonLib.response.asString();
		JsonPath js = new JsonPath(ActualResponse);
		Boolean userStatus = js.get("results.smsResult.isNewUser");
		assertEquals(isnewuser, userStatus);
	}

	@And("User validated SMSResult status as (.+)$")
	public void user_validated_smsresult_status_as(Boolean expectedSuccessStatus) throws Throwable {
		String ActualResponse = CommonLib.response.asString();
		JsonPath js = new JsonPath(ActualResponse);
		Boolean actualSuccessStatus = js.get("results.smsResult.success");
		assertEquals(expectedSuccessStatus, actualSuccessStatus);
	}

	@And("User validated the (.+) message$")
	public void user_validated_the_data(String smsresult_errormessage) throws Throwable {
		String ActualErrorMessageReceived;
		String ActualResponse = CommonLib.response.asString();
		JsonPath js = new JsonPath(ActualResponse);
		ActualErrorMessageReceived = js.get("results.smsResult.errorMessages[0]");
		System.out.println(ActualErrorMessageReceived);
		System.out.println(smsresult_errormessage);
		if (smsresult_errormessage.equals("[blank]")) {
			smsresult_errormessage = null;
			assertEquals(smsresult_errormessage, ActualErrorMessageReceived);
		} else {
			assertEquals(smsresult_errormessage, ActualErrorMessageReceived);
		}
	}

	@Given("User updated the field {string} in workbook {string} of file {string} on row {int}")
	public void user_updated_the_field_in_workbook_of_file_on_row(String field, String workBook, String filePath,
			int rowNum) {
		try {
			excelWriter = new DataDriven(System.getProperty("user.dir") + getGlobalProperty(filePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			CommonLib.writer(excelWriter, field, getGlobalProperty(workBook), rowNum);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Then("User verify Response Page")
	public void user_verify_Response_Page() {
		String ResponseReceived = CommonLib.response.asString();
		responsePageValidation(CommonLib.payloads, CommonLib.templateType, ResponseReceived);

	}

	@Given("User verify the data stored in excelfile")
	public void user_verify_terradata() {
		ValidateTerradataRecords terraValidation = new ValidateTerradataRecords();
		terraValidation.createTerradataConnection();
		FileLoggers.info("************************EDW Feed Run successfully************************");
	}

}