package fieldValidations;

import java.io.IOException;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class MobilePhone extends Utils{

	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedpayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;

	
	public void validatingAllMobilePhoneScenarios(String methodName, String resourceType, String payload) throws IOException {
		mobilePhoneExceedingMaxLength(methodName,  resourceType,  payload);
		mobilePhoneWithLessThanMinLength(methodName,  resourceType,  payload);
		mobilePhoneWithTwoHypensInvalidFormat(methodName,  resourceType,  payload);
		mobilePhoneWithAlphaNumeric(methodName,  resourceType,  payload);
//		mobilePhoneWithSingleHypensInvalidFormat(methodName,  resource,  payload);
		mobilePhoneWith12Digit(methodName,  resourceType,  payload);
		
	}
	
	
	public void mobilePhoneExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("Validating MobilePhone Fields");
		String fieldName = "mobilePhone";
		String fieldValue = "1234567890123456789011";
		ValidationMessages message = ValidationMessages.valueOf("MOBILEPHONEEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 		
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
	
//		mobilePhoneWithLessThanMinLength(methodName,  resource,  payload);
		
	}
	
	public void mobilePhoneWithLessThanMinLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("Validating MobilePhone Fields - mobilePhoneWithLessThanMinLength scenario");
		String fieldName = "mobilePhone";
		String fieldValue = "1234";
		ValidationMessages message = ValidationMessages.valueOf("MOBILEPHONEWITHLESSTHANMINLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
	}

	public void mobilePhoneWithTwoHypensInvalidFormat(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("Validating MobilePhone Fields - mobilePhoneWithTwoHypensInvalidFormat scenario");
		String fieldName = "mobilePhone";
		String fieldValue = "12-1123-1112";
		ValidationMessages message = ValidationMessages.valueOf("MOBILEPHONEWITHTWOHYPENSINVALIDFORMAT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		mobilePhoneWithAlphaNumeric(methodName,  resource,  payload);
	}
	
	public void mobilePhoneWithSingleHypensInvalidFormat(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("Validating MobilePhone Fields - mobilePhoneWithSingleHypensInvalidFormat scenario");
		String fieldName = "mobilePhone";
		String fieldValue = "123-1121212";
		ValidationMessages message = ValidationMessages.valueOf("MOBILEPHONEWITHSINGLEHYPENSINVALIDFORMAT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		mobilePhoneWithAlphaNumeric(methodName,  resource,  payload);
	}
	public void mobilePhoneWithAlphaNumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("Validating MobilePhone Fields - mobilePhoneWithAlphaNumeric scenario");
		String fieldName = "mobilePhone";
		String fieldValue = "1234a12345";
		ValidationMessages message = ValidationMessages.valueOf("MOBILEPHONEWITHALPHANUMERIC");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		mobilePhoneWith12Digit(methodName,  resource,  payload);
	}
	public void mobilePhoneWith12Digit(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("Validating MobilePhone Fields - mobilePhoneWith12Digit scenario");
		String fieldName = "mobilePhone";
		String fieldValue = "112345678912";
		ValidationMessages message = ValidationMessages.valueOf("MOBILEPHONEWITH12DIGIT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
		FileLoggers.info("Validation MobilePhone Fields Completed");
	}
	
	













}
