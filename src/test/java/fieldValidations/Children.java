package fieldValidations;

import java.io.IOException;

import common.FileLoggers;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Children extends Utils{


	RequestSpecification req;
//	ReplacefieldValuepayload pup = new ReplacefieldValuepayload();
	String updatedpayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int ExpectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expected_ErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;
	
	public void validatingAllChildrenFieldScenarios(String methodName, String resourceType, String payload) throws IOException {
		childrenFirstNameUnknownBlank(methodName,  resourceType,  payload);
		childrenFirstNameBlank(methodName,  resourceType,  payload);
		childrenHasDOBBlank(methodName,  resourceType,  payload);
		childrenDateOfBirthBlank(methodName,  resourceType,  payload);
		childrenWithFutureDateOfBirth(methodName,  resourceType,  payload);
		childrenExpectedBirthMonthBlank(methodName,  resourceType,  payload);
		childrenWithInvalidExpectedBirthMonth(methodName,  resourceType,  payload);
		childrenWithPastExpectedBirthMonth(methodName,  resourceType,  payload);
		childrenExpectedBirthYearBlank(methodName,  resourceType,  payload);
		childrenWithFutureExpectedBirthYear(methodName,  resourceType,  payload);
		childrenWithGenderNull(methodName,  resourceType,  payload);
		childrenWithGenderM(methodName,  resourceType,  payload);
		childrenWithGenderF(methodName,  resourceType,  payload);
		childrenWithGenderSurprise(methodName,  resourceType,  payload);
		childrenWithInvalidGenderValue(methodName,  resourceType,  payload);
		
	}

	public void childrenFirstNameUnknownBlank(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-FirstNameUnknown Fields childrenFirstNameUnknownBlank scenario*****");
		String fieldName = "firstNameUnknown";
		String fieldValue = " ";	
//		expectedErrorMessage = "FirstNameUnknown of child[0] must be supplied";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENFIRSTNAMEUNKNOWNBLANK");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenFirstNameBlank(methodName,  resource,  payload);
	}

	public void childrenFirstNameBlank(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenFirstNameBlank Fields*****");
		String fieldName = "Children_firstName";
		String fieldValue = "  ";	
//		expectedErrorMessage = "First Name of child[0] must be supplied";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENFIRSTNAMEBLANK");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenHasDOBBlank(methodName,  resource,  payload);
	}
	public void childrenHasDOBBlank(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenHasDOBBlank Fields*****");
		String fieldName = "hasDateOfBirth";
		String fieldValue = "  ";	
//		expectedErrorMessage = "HasDOB of child[0] must be supplied";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENHASDOBBLANK");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenDateOfBirthBlank(methodName,  resource,  payload);
	}
	public void childrenDateOfBirthBlank(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenDateOfBirthBlank Fields*****");
		String fieldName = "dateOfBirth";
		String fieldValue = "";	
//		expectedErrorMessage = "DOB of child[0] must be supplied";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENDATEOFBIRTHBLANK");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenWithFutureDateOfBirth(methodName,  resource,  payload);
	}
	public void childrenWithFutureDateOfBirth(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenWithFutureDateOfBirth Fields*****");
		String fieldName = "dateOfBirth";
		String fieldValue = "2024-06-28";	
//		expectedErrorMessage = "DOB of child[0] must be in the past";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENWITHFUTUREDATEOFBIRTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenExpectedBirthMonthBlank(methodName,  resource,  payload);
	}
	public void childrenExpectedBirthMonthBlank(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenExpectedBirthMonthBlank Fields*****");
		String fieldName = "expectedBirthMonth";
		String fieldValue = "";	
//		expectedErrorMessage = "Babys Expected Month of child[{0}] must be supplied";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENEXPECTEDBIRTHMONTHBLANK");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenWithInvalidExpectedBirthMonth(methodName,  resource,  payload);
	}
	public void childrenWithInvalidExpectedBirthMonth(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenWithInvalidExpectedBirthMonth Fields*****");
		String fieldName = "expectedBirthMonth";
		String fieldValue = "18";	
//		expectedErrorMessage = "Babys Expected Month of child[{0}] is invalid";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENWITHINVALIDEXPECTEDBIRTHMONTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenWithPastExpectedBirthMonth(methodName,  resource,  payload);
	}
	public void childrenWithPastExpectedBirthMonth(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenWithPastExpectedBirthMonth Fields*****");
		String fieldName = "expectedBirthMonth";
		String fieldValue = "1";	
//		expectedErrorMessage = "Expected date of child[0] must be in the future";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENWITHPASTEXPECTEDBIRTHMONTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenExpectedBirthYearBlank(methodName,  resource,  payload);
	}
	public void childrenExpectedBirthYearBlank(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenExpectedBirthYearBlank Fields*****");
		String fieldName = "expectedBirthYear";
		String fieldValue = "0";	
//		expectedErrorMessage = "Expected Year of child[0] is required";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENEXPECTEDBIRTHYEARBLANK");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenWithFutureExpectedBirthYear(methodName,  resource,  payload);
	}
	public void childrenWithFutureExpectedBirthYear(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenWithFutureExpectedBirthYear Fields*****");
		String fieldName = "expectedBirthYear";
		String fieldValue = "2024";	
//		expectedErrorMessage = "Expected date of child[0] must be in the future";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENWITHFUTUREEXPECTEDBIRTHYEAR");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenWithGenderNull(methodName,  resource,  payload);
	}
	public void childrenWithGenderNull(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenWithGenderNull Fields*****");
		String fieldName = "gender";
		String fieldValue = null;	
//		expectedErrorMessage = "Gender of child[0] must be supplied";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENWITHGENDERNULL");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		childrenWithGenderM(methodName,  resource,  payload);
	}
	public void childrenWithGenderM(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenWithGenderM Fields*****");
		String fieldName = "gender";
		String fieldValue = "m";	
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
//		childrenWithGenderF(methodName,  resource,  payload);
	}
	public void childrenWithGenderF(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenWithGenderF Fields*****");
		String fieldName = "gender";
		String fieldValue = "f";	
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
//		childrenWithGenderSurprise(methodName,  resource,  payload);
	}
	public void childrenWithGenderSurprise(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenWithGenderSurprise Fields*****");
		String fieldName = "gender";
		String fieldValue = "Surprise";	
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
//		childrenWithInvalidGenderValue(methodName,  resource,  payload);
	}
	public void childrenWithInvalidGenderValue(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Children-childrenWithInvalidGenderValue Fields*****");
		String fieldName = "gender";
		String fieldValue = "invalid";	
//		expectedErrorMessage = "Gender of child[0]  is invalid";
		ValidationMessages message = ValidationMessages.valueOf("CHILDRENWITHINVALIDGENDERVALUE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		(methodName,  resource,  payload);
		FileLoggers.info("Validation Children Fields Completed");
	}







}
