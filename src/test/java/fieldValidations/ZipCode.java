package fieldValidations;



import java.io.IOException;
import org.json.JSONObject;
import org.junit.Assert;
import org.testng.asserts.SoftAssert;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class ZipCode extends Utils{

	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedpayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;
	
	
	public void validatingAllZipCodeScenarios(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*********Validating Zipcode Fields - All Scenarios *****");
		zipExceedingMaxLength(methodName,resourceType,payload);
		zipWithLessThanMaxLength(methodName,resourceType,payload);
		zipWithCharAndDigitInValidValue(methodName,  resourceType,  payload);
		zipWithSpecialCharacters(methodName,  resourceType,  payload);
		zipWithAlphaNumeric(methodName,  resourceType,  payload);
		zipWithNumericValue(methodName,  resourceType,  payload);
		zipWithNumericAndCharValue(methodName,  resourceType,  payload);
		zipWith10NumericValue(methodName,  resourceType,  payload);
		
	}
	

	public void zipExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Zipcode Fields - *****");
		String fieldName = "zipcode";
		String fieldValue = "900019000190";		
		ValidationMessages message = ValidationMessages.valueOf("ZIPEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		zipWithLessThanMaxLength(methodName,  resource,  payload);
	}
	
	public void zipWithLessThanMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Zipcode Fields - zipWithLessThanMaxLength scenario*****");
		String fieldName = "zipcode";
		String fieldValue = "900";		
		ValidationMessages message = ValidationMessages.valueOf("ZIPWITHLESSTHANMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		zipWithCharAndDigitInValidValue(methodName,  resource,  payload);
	}

	public void zipWithCharAndDigitInValidValue(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Zipcode Fields - zipWithCharAndDigitInValidValue scenario*****");
		String fieldName = "zipcode";
		String fieldValue = "123-5ABCDE";
		ValidationMessages message = ValidationMessages.valueOf("ZIPWITHCHARANDDIGITINVALIDVALUE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		zipWithSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void zipWithSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Zipcode Fields - zipWithSpecialCharacters scenario*****");
		String fieldName = "zipcode";
		String fieldValue = "A@";
		ValidationMessages message = ValidationMessages.valueOf("ZIPWITHSPECIALCHARACTERS");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		zipWithAlphaNumeric(methodName,  resource,  payload);
	}
	public void zipWithAlphaNumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Zipcode Fields - zipWithAlphaNumeric scenario*****");
		String fieldName = "zipcode";
		String fieldValue = "A1";
		ValidationMessages message = ValidationMessages.valueOf("ZIPWITHALPHANUMERIC");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		zipWithNumericValue(methodName,  resource,  payload);
	}
	public void zipWithNumericValue(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Zipcode Fields - zipWithNumericValue scenario*****");
		String fieldName = "zipcode";
		String fieldValue = "11";
		ValidationMessages message = ValidationMessages.valueOf("ZIPWITHNUMERICVALUE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		zipWithNumericAndCharValue(methodName,  resource,  payload);
	}
	public void zipWithNumericAndCharValue(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("****Validating Zipcode Fields - zipWithNumericAndCharValue scenario*****");
		FileLoggers.info("Zipcode with Starting 5 Characters and last 4 Digits");
		String fieldName = "zipcode";
		String fieldValue = "12345ABCD";
		ValidationMessages message = ValidationMessages.valueOf("ZIPWITHNUMERICANDCHARVALUE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		zipWith10NumericValue(methodName,  resource,  payload);
	}
	public void zipWith10NumericValue(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Zipcode Fields - zipWith10NumericValue scenario*****");
		FileLoggers.info("Zipcode with Starting 5 Characters and last 4 Digits");
		String fieldName = "zipcode";
		String fieldValue = "12345-6789";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);		
		FileLoggers.info("Validation Zipcode Fields Completed");
	}
	
	
	
}
