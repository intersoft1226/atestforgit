package fieldValidations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class City extends Utils{

	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String ExpectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String Expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;
	
	public void validatingAllCityScenarios(String methodName, String resourceType, String payload) throws IOException {
		cityExceedingMaxLength(methodName,  resourceType,  payload);
		cityAcceptingAlphanumeric(methodName,  resourceType,  payload);
		cityAcceptingSpecialCharacters(methodName,  resourceType,  payload);
	}

	public void cityExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating City Fields - cityExceedingMaxLength Scenario*****");
		String fieldName = "city";
		String fieldValue = "citycitycitywithexceedingmaxlengthinrequestformatforfieldvalidation";	
//		expectedErrorMessage = "City Field should be max length 50";
		ValidationMessages message = ValidationMessages.valueOf("CITYEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		cityAcceptingAlphanumeric(methodName,  resource,  payload);
	}

	public void cityAcceptingAlphanumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating City Fields - cityAcceptingAlphanumeric scenario*****");
		String fieldName = "city";
		String fieldValue = "city12345";		
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);		
//		cityAcceptingSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void cityAcceptingSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating City Fields - cityAcceptingSpecialCharacters scenario*****");
		String fieldName = "city";
		String fieldValue = "city!@#$%^&*(){~!12345";	
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
		FileLoggers.info("Validation City Fields Completed");
	}
	
	






}
