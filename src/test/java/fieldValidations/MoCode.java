package fieldValidations;

import java.io.IOException;

import common.FileLoggers;
import common.Utils;
import common.ValidationMessages;

public class MoCode extends Utils{
	String expectedErrorMessage;
	
	public void validatingAllMoCodeScenarios(String methodName, String resourceType, String payload) throws IOException {
		moCodeExceedingMaxLength(methodName, resourceType, payload);
		moCodeLessThanMinimumLength(methodName, resourceType, payload);
		moCodeWithAlphanumeric(methodName, resourceType, payload);
		moCodeWithSpecialCharacters(methodName, resourceType, payload);
		moCodeWithHyphenValidFormat(methodName, resourceType, payload);
		moCodeWithHyphenInValidFormat(methodName, resourceType, payload);
	}
	
	public void moCodeExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating MoCode Fields moCodeExceedingMaxLength scenario*****");
		String fieldName = "mocode";
		String fieldValue = "113456789";
		ValidationMessages message = ValidationMessages.valueOf("MOCODEEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		moCodeLessThanMinimumLength(methodName, resource, payload);
	}
	
	public void moCodeLessThanMinimumLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating MoCode Fields - moCodeLessThanMinimumLength scenario*****");
		String fieldName = "mocode";
		String fieldValue = "11";
		ValidationMessages message = ValidationMessages.valueOf("MOCODELESSTHANMINIMUMLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		moCodeWithAlphanumeric(methodName, resource, payload);
	}
	public void moCodeWithAlphanumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating MoCode Fields - moCodeWithAlphanumeric scenario*****");
		String fieldName = "mocode";
		String fieldValue = "11B4567A";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
//		moCodeWithSpecialCharacters(methodName, resource, payload);
	}
	public void moCodeWithSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating MoCode Fields - moCodeWithSpecialCharacters scenario*****");
		String fieldName = "mocode";
		String fieldValue = "11B4567@";
		ValidationMessages message = ValidationMessages.valueOf("MOCODEWITHSPECIALCHARACTERS");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		moCodeWithHyphenValidFormat(methodName, resource, payload);
	}
	public void moCodeWithHyphenValidFormat(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating MoCode Fields - moCodeWithHyphenValidFormat scenario*****");
		String fieldName = "mocode";
		String fieldValue = "AAAA-AAAA";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
//		moCodeWithHyphenInValidFormat(methodName, resource, payload);
	}
	public void moCodeWithHyphenInValidFormat(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating MoCode Fields - moCodeWithHyphenInValidFormat scenario*****");
		String fieldName = "mocode";
		String fieldValue = "AA-AAAAAA";
		ValidationMessages message = ValidationMessages.valueOf("MOCODEWITHHYPHENINVALIDFORMAT");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);	
		FileLoggers.info("Validation MoCode Fields Completed");
	}

}
