package fieldValidations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class State extends Utils{
	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedpayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String ExpectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String Expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;

	public void validatingAllStateScenarios(String methodName, String resourceType, String payload) throws IOException {
		stateExceedingMaxLength(methodName,  resourceType,  payload);
		stateWithLessThanMinLength(methodName,  resourceType,  payload);
		stateWithNonCapitalValidValue(methodName,  resourceType,  payload);
		stateWithSpecialCharacters(methodName,  resourceType,  payload);
		stateWithAlphaNumeric(methodName,  resourceType,  payload);
		stateWithNumericValue(methodName,  resourceType,  payload);
		
	}

	public void stateExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating State Fields stateExceedingMaxLength scenario*****");
		String fieldName = "state";
		String fieldValue = "AZZ";
		ValidationMessages message = ValidationMessages.valueOf("STATEEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		stateWithLessThanMinLength(methodName,  resource,  payload);
	}
	
	public void stateWithLessThanMinLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating State Fields - stateWithLessThanMinLength scenario*****");
		String fieldName = "state";
		String fieldValue = "A";
		ValidationMessages message = ValidationMessages.valueOf("STATEWITHLESSTHANMINLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		stateWithNonCapitalValidValue(methodName,  resource,  payload);
	}

	public void stateWithNonCapitalValidValue(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating State Fields - stateWithNonCapitalValidValue scenario*****");
		String fieldName = "state";
		String fieldValue = "al";
		ValidationMessages message = ValidationMessages.valueOf("STATEWITHNONCAPITALVALIDVALUE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		stateWithSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void stateWithSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating State Fields - stateWithSpecialCharacters scenario*****");
		String fieldName = "state";
		String fieldValue = "A@";
		ValidationMessages message = ValidationMessages.valueOf("STATEWITHSPECIALCHARACTERS");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		stateWithAlphaNumeric(methodName,  resource,  payload);
	}
	public void stateWithAlphaNumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating State Fields - stateWithAlphaNumeric scenario*****");
		String fieldName = "state";
		String fieldValue = "A1";
		ValidationMessages message = ValidationMessages.valueOf("STATEWITHALPHANUMERIC");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		stateWithNumericValue(methodName,  resource,  payload);
	}
	public void stateWithNumericValue(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating State Fields - stateWithNumericValue scenario*****");
		String fieldName = "state";
		String fieldValue = "11";
		ValidationMessages message = ValidationMessages.valueOf("STATEWITHNUMERICVALUE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
		FileLoggers.info("Validation State Fields Completed");
	}
	
	










}
