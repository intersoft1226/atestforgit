package fieldValidations;

import java.io.IOException;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class BounceXCustomFields extends Utils{
	RequestSpecification req;
//	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedPayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expected_ErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;

	public void CmpDeploymentStrategyMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating CmpDeploymentStrategy Fields*****");
		String fieldName = "CmpDeploymentStrategy";
		String fieldValue = "CmpDeploymentStrategywithexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("CMPDEPLOYMENTSTRATEGYMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);

	}
	public void CmpNameMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating CmpNameMaxLength Fields*****");
		String fieldName = "CmpName";
		String fieldValue = "CmpNamewithexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("CMPNAMEMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
		
	}
	public void CmpVariationIDMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating CmpVariationIDMaxLength Fields*****");
		String fieldName = "CmpVariationID";
		String fieldValue = "cmpVariationIDwithexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("CMPVARIATIONIDMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
	
	}
	public void CmpConceptMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating CmpConceptMaxLength Fields*****");
		String fieldName = "CmpConcept";
		String fieldValue = "CmpConceptwithexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("CMPCONCEPTMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
		
	}
	public void CmpDeviceMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating CmpDeviceMaxLength Fields*****");
		String fieldName = "CmpDevice";
		String fieldValue = "CmpDevicewithexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("CMPDEVICEMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
		
	}
	public void CmpFormatMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating CmpFormatMaxLength Fields*****");
		String fieldName = "CmpFormat";
		String fieldValue = "CmpFormatwithexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidationexceedingmaxlengthinrequestformatforfieldvalidation";
		ValidationMessages message = ValidationMessages.valueOf("CMPFORMATMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
	}
	
	
	
	
}
