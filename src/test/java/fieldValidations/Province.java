package fieldValidations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Province extends Utils{


	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedpayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String ExpectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String Expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;

	public void validatingAllProvinceScenarios(String methodName, String resourceType, String payload) throws IOException {
		provinceExceedingMaxLength(methodName,  resourceType,  payload);
		provinceAcceptingAlphanumeric(methodName,  resourceType,  payload);
		provinceAcceptingSpecialCharacters(methodName,  resourceType,  payload);
		provinceWithInvalidValue(methodName,  resourceType,  payload);
		
	}

	public void provinceExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Province Fields provinceExceedingMaxLength scenario*****");
		String fieldName = "province";
		String fieldValue = "ABB";
		ValidationMessages message = ValidationMessages.valueOf("PROVINCEEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		provinceAcceptingAlphanumeric(methodName,  resource,  payload);
	}

	public void provinceAcceptingAlphanumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Province Fields - provinceAcceptingAlphanumeric scenario*****");
		String fieldName = "province";
		String fieldValue = "A5";
		ValidationMessages message = ValidationMessages.valueOf("PROVINCEACCEPTINGALPHANUMERIC");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);		
//		provinceAcceptingSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void provinceAcceptingSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Province Fields - provinceAcceptingSpecialCharacters scenario*****");
		String fieldName = "province";
		String fieldValue = "A@";
		ValidationMessages message = ValidationMessages.valueOf("PROVINCEACCEPTINGSPECIALCHARACTERS");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		provinceWithInvalidValue(methodName,  resource,  payload);
	}
	
	public void provinceWithInvalidValue(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Province Fields - provinceWithInvalidValue scenario*****");
		String fieldName = "province";
		String fieldValue = "RG";
		ValidationMessages message = ValidationMessages.valueOf("PROVINCEWITHINVALIDVALUE");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
		FileLoggers.info("Validation Province Fields Completed");
	}
	





}
