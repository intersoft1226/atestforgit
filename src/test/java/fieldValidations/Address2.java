package fieldValidations;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.json.JSONObject;
import org.junit.Assert;

import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Address2 extends Utils{

	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	Response response;
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage;
	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
	String responsePage;

	public void validatingAllAddress2Scenarios(String methodName, String resourceType, String payload) throws IOException {
		address2ExceedingMaxLength(methodName,  resourceType,  payload);
		address2AcceptingAlphanumeric(methodName,  resourceType,  payload);
		address2AcceptingSpecialCharacters(methodName,  resourceType,  payload);
		
	}
	
	public void address2ExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Address2 Fields address2ExceedingMaxLength Scenario*****");
		String fieldName = "addr2";
		String fieldValue = "address2withexceedingmaxlengthinrequestformatforfieldvalidation";		
		ValidationMessages message = ValidationMessages.valueOf("ADDRESS2EXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage(); 
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		address2AcceptingAlphanumeric(methodName,  resource,  payload);
	}

	public void address2AcceptingAlphanumeric(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Address2 Fields - address2AcceptingAlphanumeric scenario*****");
		String fieldName = "addr2";
		String fieldValue = "address12345";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);		
//		address2AcceptingSpecialCharacters(methodName,  resource,  payload);
	}
	
	public void address2AcceptingSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
		FileLoggers.info("*****Validating Address2 Fields - address2AcceptingSpecialCharacters scenario*****");
		String fieldName = "addr2";
		String fieldValue = "address!@#$%^&*(){}~!12345";
		expectedErrorMessage = "";
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 200, expectedErrorMessage);
		FileLoggers.info("Validation Address2 Fields Completed");
	}
	
	

}
