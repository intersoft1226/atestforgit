package fieldValidations;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONObject;

import common.CommonLib;
import common.DataBaseConnection;
import common.FileLoggers;
import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import stepDefinations.TemplateTypeStepDefs;

public class CustomFields extends Utils {
	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	CustomCheckBox cb = new CustomCheckBox();
	
	String updatedPayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Baby Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Baby Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;
	String fieldType;
	String result;
	String isRequired;

	public void validateCustomType(String methodName, String resourceType, String payloads, String customFieldID,
			String customFieldValue, int setCount) throws IOException, SQLException {
		int flag = 0;
		CommonLib.customFieldId = customFieldID;
		JSONObject jsobj = new JSONObject(payloads);
		templateId = jsobj.getString("id");
//		DataBaseConnection db = new DataBaseConnection();		
		String fieldType;
		String values;
		int totalNumberOfvalues = 0;
		String query = "SELECT fieldType FROM FIELD WHERE FieldId="+ "'" + CommonLib.customFieldId + "'";
		ResultSet rs1 = CommonLib.db.getSQLResults(query);	
		if(rs1.next()) {
		fieldType = rs1.getString(1);
		FileLoggers.info("***fieldType is********   "+fieldType);		
		}
		//********* Fetching Custom fields Label *****************
		TemplateTypeStepDefs tem= new TemplateTypeStepDefs();
		if(CommonLib.customFieldData==null) {
			fetchCustomFieldLabelData();
		}
		CommonLib.customFieldLabel= tem.getCustomFieldLabelName(CommonLib.customFieldId);
		//********************
		
		//query = "SELECT Required FROM FORM_FIELD WHERE FieldId="+ "'" + CommonLib.customFieldId + "'";
		query ="SELECT required FROM form_field ff INNER JOIN Template t ON ff.templatekey = t.templatekey WHERE t.templateId ="+"'"+templateId+"'"+" AND ff.FieldId = "+"'"+CommonLib.customFieldId+"'";                                       
		FileLoggers.info("*******Query for fetching Required or Not -> "+query);
		ResultSet rs2 = CommonLib.db.getSQLResults(query);	
		if(rs2.next()) {
		try {
		isRequired= rs2.getString(1);	
		}catch(Exception e) {
			System.out.println(e);
			isRequired="0";
		}
		}
		FileLoggers.info("Required is "+isRequired);		
		query = "SELECT VALUE FROM field_option WHERE FieldId="+ "'" + CommonLib.customFieldId + "'";
		ResultSet rs3 = CommonLib.db.getSQLResults(query);	
		if(rs3.first()) {
		values= rs3.getString(1);
		totalNumberOfvalues = getRows(rs3);	
		FileLoggers.info("values is "+values);
		}FileLoggers.info("TotalNumberOfvalues is "+totalNumberOfvalues);
		
		
		// Check for TextBox
		FileLoggers.info("*****************TextBox Custom Field Check**********************");
		String TextBoxfieldTypeCheckQuery = "SELECT fieldType FROM FIELD WHERE FieldId = " + "'" + CommonLib.customFieldId + "'";
		FileLoggers.info("Textbox check query generated - " + TextBoxfieldTypeCheckQuery);		
		ResultSet rs = CommonLib.db.getSQLResults(TextBoxfieldTypeCheckQuery);
		if (rs != null) {
			while (rs.next()) {
				result = rs.getString("fieldType");
				if (result.equals("text")) {
//					isRequired = rs.getString("required");
//					FileLoggers.info("Custom fieldType is : " + result);
//					
					if (isRequired.equals("1")) {						
//						expectedErrorMessage = customFieldID + " Field is required";
						expectedErrorMessage = CommonLib.customFieldLabel + " Field is required";
						cb.customMissingID(methodName, resourceType, payloads, CommonLib.customFieldId, customFieldValue, setCount,
								expectedErrorMessage, flag);
						return;
					} else {
						expectedErrorMessage = "Custom Id is missing or invalid";
						cb.customMissingID(methodName, resourceType, payloads, CommonLib.customFieldId, customFieldValue, setCount,
								expectedErrorMessage, flag);
						return;
					}
				}

			}
		}
		
		
		FileLoggers.info("Custom fieldType is : " + result);
		if (isRequired.equals("1")) {
			flag=1;
			if (result.equals("checkbox")) {
//				expectedErrorMessage = customFieldID + " Field is required";
				expectedErrorMessage = CommonLib.customFieldLabel + " Field is required";
				
				cb.customMissingID(methodName, resourceType, payloads, CommonLib.customFieldId, customFieldValue, setCount,
						expectedErrorMessage, flag);
				if (totalNumberOfvalues > 1) {
					cb.customMultipleValueCheck(methodName, resourceType, payloads, CommonLib.customFieldId, customFieldValue,
							setCount);
				}
				return;
			} else {
//				expectedErrorMessage = customFieldID + " Field is required";
				expectedErrorMessage = CommonLib.customFieldLabel + " Field is required";
				cb.customMissingID(methodName, resourceType, payloads, CommonLib.customFieldId, customFieldValue, setCount,
						expectedErrorMessage, flag);
				return;
			}
		} else {
			expectedErrorMessage = "Custom Id is missing or invalid";
			cb.customMissingID(methodName, resourceType, payloads, CommonLib.customFieldId, customFieldValue, setCount,
					expectedErrorMessage, flag);
		}
	
		
				
//		FileLoggers.info("****** Checking Custom fieldType ******");
//		FileLoggers.info("Query is " + query);
//		ResultSet rsa = db.dbConnection(query);
//		int TotalNumberOfvalues = getRows(rsa);		
//		if (rsa != null) {
//			while(rsa.next()) {
//				FileLoggers.info("*********Inside check*********");
//				result = rsa.getString("fieldType");
//				isRequired = rsa.getString("required");
//				FileLoggers.info("Custom fieldType is : " + result);
//				if (isRequired.equals("1")) {
//					if (result.equals("checkbox")) {
//						expectedErrorMessage = customFieldID + " Field is required";
//						cb.customMissingID(methodName, resourceType, payloads, customFieldID, cCustomFieldValue, setCount,
//								expectedErrorMessage, flag);
//						if (TotalNumberOfvalues > 1) {
//							cb.customMultipleValueCheck(methodName, resourceType, payloads, customFieldID, cCustomFieldValue,
//									setCount);
//						}
//						return;
//					} else {
//						expectedErrorMessage = customFieldID + " Field is required";
//						cb.customMissingID(methodName, resourceType, payloads, customFieldID, cCustomFieldValue, setCount,
//								expectedErrorMessage, flag);
//						return;
//					}
//				} else {
//					expectedErrorMessage = "Custom Id is missing or invalid";
//					cb.customMissingID(methodName, resourceType, payloads, customFieldID, cCustomFieldValue, setCount,
//							expectedErrorMessage, flag);
//				}
//			}
		}
	}

