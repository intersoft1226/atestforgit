package fieldValidations;


import java.io.IOException;

import common.ReplaceFieldValuePayload;
import common.RequestAPI;
import common.ResponseAPI;
import common.Utils;
import common.ValidationMessages;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Guardian extends Utils {
	RequestSpecification req;
	ReplaceFieldValuePayload pup = new ReplaceFieldValuePayload();
	String updatedpayload;
	Response response;
	RequestAPI api = new RequestAPI();
	ResponseAPI respapi = new ResponseAPI();
	int expectedStatusCode = 400;
	String requestId;
	String responseId;
	String templateId;
	String expectedErrorMessage = "Email Field format is invalid";
//	String expectedErrorMessage = " Field value is invalid";
//	String expectedMaxLengthErrorMessage = "Email Field should be max length 128";
//	String expected_MaxLengthErrorMessage = " Field should be max length 50";
	String responsePage;
	
	
	public void validatingAllGuardianScenarios(String methodName, String resourceType, String payload) throws IOException {
	guardianExceedingMaxLength(methodName, resourceType, payload);
	guardianExceedingMinLength(methodName, resourceType, payload);	
	}

	public void guardianExceedingMaxLength(String methodName, String resourceType, String payload) throws IOException {
		String fieldName = "guardian";
		String fieldValue = "11";
		ValidationMessages message = ValidationMessages.valueOf("GUARDIANEXCEEDINGMAXLENGTH");
		expectedErrorMessage = message.getValidationMessage();      
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
		
	}
	public void guardianExceedingMinLength(String methodName, String resourceType, String payload) throws IOException {
		String fieldName = "guardian";
		String fieldValue = "0";
		ValidationMessages message = ValidationMessages.valueOf("GUARDIANEXCEEDINGMINLENGTH");
		expectedErrorMessage = message.getValidationMessage();       
		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
		
	}
	
	//*******UnHandled Scenaio*******
//	public void guardianWithAlphanumeric(String methodName, String resourceType, String payload) throws IOException {
//		String fieldName = "guardian";
//		String fieldValue = "A";
//		expectedErrorMessage = "Guardian Field value is invalid";
//		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//		guardianWithSpecialCharacters(methodName, resourceType, payload);
//	}
	
//	public void guardianWithInvalidValue(String methodName, String resourceType, String payload) throws IOException {
//	String fieldName = "guardian";
//	String fieldValue = "RG";
//	expectedErrorMessage = "Guardian Field value is invalid";
//	responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//}

//	public void guardianWithSpecialCharacters(String methodName, String resourceType, String payload) throws IOException {
//		String fieldName = "guardian";
//		String fieldValue = "@";
//		expectedErrorMessage = "Guardian Field value is invalid";
//		responseValidation(payload, fieldName, fieldValue, resourceType, methodName, 400, expectedErrorMessage);
//	
//	}


}
